﻿// See https://aka.ms/new-console-template for more information
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Security.Cryptography;

Console.Write($"Worming Up");
Enumerable.Repeat(1024, Environment.ProcessorCount * 1000).AsParallel().ForAll(Test);
Console.WriteLine($"Finished!");

await Task.Delay(1000); 

foreach (var size in Enumerable.Range(1, 16).Select(x => x * 512))
{
    gen.Clear();
    exc.Clear();
    enc.Clear();
    dec.Clear();

    Console.WriteLine("".PadLeft(116, '='));
    Console.WriteLine($"KEYSIZE: {size}bit");
    var tim = Stopwatch.StartNew();
    Enumerable.Repeat(size, 100).AsParallel().ForAll(Test);
    tim.Stop();
    Console.WriteLine("OK!");
    Console.WriteLine($"TIM_GEN: {gen.Average(x => x.TotalMilliseconds):0}ms");
    Console.WriteLine($"TIM_EXC: {exc.Average(x => x.TotalMilliseconds):0}ms");
    Console.WriteLine($"TIM_ENC: {enc.Average(x => x.TotalMilliseconds):0}ms");
    Console.WriteLine($"TIM_DEC: {dec.Average(x => x.TotalMilliseconds):0}ms");
    Console.WriteLine($"TIM_ALL: {tim.Elapsed.TotalMilliseconds:0}ms");
}

static void Test(int keySize)
{
    #region clientInit
    var gen = Stopwatch.StartNew();
    var rsaclient = new RSACryptoServiceProvider(keySize);
    var pub = rsaclient.ExportParameters(false);
    gen.Stop();
    #endregion

    var mod = pub.Modulus;
    var exp = pub.Exponent;

    #region serverInit
    var exc = Stopwatch.StartNew();
    var rsaserver = new RSACryptoServiceProvider();
    rsaserver.ImportParameters(new() { Exponent = exp, Modulus = mod });
    exc.Stop();
    #endregion

    var svtk = RandomNumberGenerator.GetBytes(rsaserver.KeySize / 8 - 11);

    #region serverEncrypt
    var enc = Stopwatch.StartNew();
    var data = rsaserver.Encrypt(svtk, false);
    enc.Stop();
    #endregion

    #region clientDecrypt
    var dec = Stopwatch.StartNew();
    var cltk = rsaclient.Decrypt(data, false);
    dec.Stop();
    #endregion

    Program.gen.Add(gen.Elapsed);
    Program.exc.Add(exc.Elapsed);
    Program.enc.Add(enc.Elapsed);
    Program.dec.Add(dec.Elapsed);

    Console.Write(".");
}

partial class Program
{
    static readonly ConcurrentBag<TimeSpan> gen = new();
    static readonly ConcurrentBag<TimeSpan> exc = new();
    static readonly ConcurrentBag<TimeSpan> enc = new();
    static readonly ConcurrentBag<TimeSpan> dec = new();
}
